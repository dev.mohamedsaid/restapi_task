<?php

namespace App\Http\Controllers\APIControllers\Tasks;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Traits\ResponseData;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    use ResponseData;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data =  TaskResource::collection(Task::get());
        return $this->successData('all tasks' , $data);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(TaskRequest $request)
    {
        $created = Task::create($request->validated());
        return $this->successData('Task Created Successfully',$created);
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        $data =  new TaskResource($task);
        return $this->successData('Show Single Task' , $data);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(TaskRequest $request, Task $task)
    {
        $task->update($request->validated());
        return $this->successData('Task Updated Successfully',$task);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return $this->successData('Task Deleted Successfully');
    }
}
