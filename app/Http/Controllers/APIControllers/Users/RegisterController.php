<?php

namespace App\Http\Controllers\APIControllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Traits\ResponseData;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    use ResponseData;

    //user register
    public function register(RegisterRequest $request)
    {
        User::create($request->validated());
        return $this->successData('User Created Successfully');
    }
}
