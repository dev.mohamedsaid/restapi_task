<?php

namespace App\Http\Controllers\APIControllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Traits\ResponseData;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use ResponseData;
    //User Login
    public function login(LoginRequest $request)
    {
        $token = auth()->attempt($request->validated());
        if (!$token)
        {
            return $this->failureData('Email or Password is invalid', 401);
        }
        return response()->json([
            'success'=>true,
            'access_token' => $token,
            'user' => auth()->user(),
        ]);
    }



}
