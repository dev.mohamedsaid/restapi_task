<?php

namespace App\Http\Controllers\APIControllers\Users;

use App\Http\Controllers\Controller;
use App\Traits\ResponseData;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ResponseData;


    public function get_user_data()
    {
        $user = auth()->user();
        return $this->successData('User Data', $user);
    }

    //Logout User
    public function logout()
    {
        auth()->logout();
        return $this->successData('Successfully logged out');
    }
}
