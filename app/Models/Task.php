<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'description',
        'status'
    ];


    //Get Status Attribute
    public function getStatusAttribute($value)
    {
        return ($value == 1) ? 'active' : 'unactive';
    }

}
