<?php
namespace App\Traits;


trait ResponseData
{
    protected function successData($message, $data = [], $status = 200)
    {
        return response([
            'success' => true,
            'message' => $message,
            'data' => $data,
        ], $status);
    }
    protected function failureData($message, $status = 422)
    {
        return response([
            'success' => false,
            'message' => $message,
        ], $status);
    }


}
