<?php

use App\Http\Controllers\APIControllers\Users\AuthController;
use App\Http\Controllers\APIControllers\Users\LoginController;
use App\Http\Controllers\APIControllers\Users\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Note That:
//Paths of API Controllers (NAMESPACE) :  App\Http\Controllers\APIControllers

Route::namespace('App\Http\Controllers\APIControllers')->group(function () {


    //******************** Users Register And Login ***************************
    Route::post('register', [RegisterController::class, 'register'])->name('users.register');
    Route::post('login', [LoginController::class, 'login'])->name('users.login');
    //******************** END Users Register And Login ************************



    //******************** Users Authentication ***************************
    Route::group(['middleware' => 'jwt.verify'], function () {
        Route::get('get_user_data', [AuthController::class, 'get_user_data'])->name('users.get_user_data');
        Route::post('logout', [AuthController::class, 'logout'])->name('users.logout');
    });
    //******************** End Users Authentication ***************************



    //******************** Tasks Resources ***************************
    Route::resource('tasks', 'Tasks\TaskController');
    //******************** End Tasks Resources ***************************


});
