<?php

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



//Test For Query Builder
//
//Route::get('/part_2_task_1', function () {
//
//    $top_5_purchased_products = \DB::table('products')
//            ->join('orders', 'products.id', '=', 'orders.product_id')
//            ->select(
//                [
//                    'products.id as product_id',
//                    'products.product_name',
//                    DB::raw('SUM(orders.amount) as total_amount_sold'),
//                    DB::raw('COUNT(DISTINCT(orders.id)) as count_this_product_in_orders'),
//                    'avg_rating' => \DB::table('ratings')->select(DB::raw('IF(ROUND(AVG(CAST(ratings.rate AS FLOAT)), 2) > 0, ROUND(AVG(CAST(ratings.rate AS FLOAT)), 2), 0)'))->whereColumn('product_id', 'products.id')
//                ]
//            )
//            ->orderByDesc('count_this_product_in_orders')
//            ->orderByDesc('total_amount_sold')
//            ->groupBy('products.id')
//            ->limit(5)
//            ->get();
//
//
//    return $top_5_purchased_products;
//});
//
//
////Part 3
//Route::get('/part_3', function () {
//
//    $refactor =  \DB::table('orders AS o')
//        ->select('*')
//        ->join('order_items AS oi', 'o.id', '=', 'oi.order_id')
//        ->join('products AS p', 'oi.product_id', '=', 'p.id')
//        ->join('categories AS c', 'p.category_id', '=', 'c.id')
//        ->where('c.name', '=', 'Electronics')
//        ->where('o.created_at', '>', now()->subDays(30))
//        ->orderBy('o.created_at', 'DESC')
//        ->limit(10)
//        ->get();
//
//    return $refactor;
//});
//
//
