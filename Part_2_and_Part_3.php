<?php

/*
 * PART 2 TASK 1 :
 * ========
 * - Write a query that retrieves all users who have made a purchase in the last 30 days.
 * - Include their names, email addresses, and the total amount they have spent.
 * ================================================================================
 * I have two tables : users and orders
 * users table  : id - name - email - address
 * orders table : id - user_id - product_id - amount - created_at
 */

$users = DB::table('users')
    ->join('orders', 'users.id', '=', 'orders.user_id')
    ->where('orders.created_at', '>=', \Carbon\Carbon::now()->subDays(30))
    ->select(
        'users.id',
        'users.name',
        'users.email',
        'users.address',
        DB::raw('SUM(orders.amount) as total_amount_spent')
    )
    ->groupBy('users.id')
    ->get();

/*
 * =======================FINISHED===================================================
 */


/*
 * PART 2 TASK 2 :
 * ========
 * - Write a query that retrieves the top 5 most purchased products.
 * - Include the product name, total quantity sold, and the average rating.
 * ================================================================================
 * I have three tables : products and orders and ratings
 * products table  : id - product_name - product_price
 * orders table    : id - user_id - product_id - amount - created_at
 * ratings table   : id - user_id - product_id - rate(from 1 to 5)
 */

$top_5_purchased_products = \DB::table('products')
    ->join('orders', 'products.id', '=', 'orders.product_id')
    ->select(
        [
            'products.id as product_id',
            'products.product_name',
            DB::raw('SUM(orders.amount) as total_amount_sold'),
            DB::raw('COUNT(DISTINCT(orders.id)) as count_this_product_in_orders'),
            'avg_rating' => \DB::table('ratings')->select(DB::raw('IF(ROUND(AVG(CAST(ratings.rate AS FLOAT)), 2) > 0, ROUND(AVG(CAST(ratings.rate AS FLOAT)), 2), 0)'))->whereColumn('product_id', 'products.id')
        ]
    )
    ->orderByDesc('count_this_product_in_orders')
    ->orderByDesc('total_amount_sold')
    ->groupBy('products.id')
    ->limit(5)
    ->get();
/*
 * =======================FINISHED===================================================
 */


/*
 * PART 3 :
 * ========
 * SELECT *
    FROM orders o
    JOIN order_items oi ON o.id = oi.order_id
    JOIN products p ON oi.product_id = p.id
    JOIN categories c ON p.category_id = c.id
    WHERE c.name = 'Electronics'
    AND o.created_at > DATE_SUB(NOW(), INTERVAL 30 DAY)
    ORDER BY o.created_at DESC
    LIMIT 10;
 * ================================================================================
 */

// Add an index on the category_id column in the products table.
// Add an index on the created_at column in the orders table.

$refactor =  \DB::table('orders AS o')
    ->select('*')
    ->join('order_items AS oi', 'o.id', '=', 'oi.order_id')
    ->join('products AS p', 'oi.product_id', '=', 'p.id')
    ->join('categories AS c', 'p.category_id', '=', 'c.id')
    ->where('c.name', '=', 'Electronics')
    ->where('o.created_at', '>', \Carbon\Carbon::now()->subDays(30))
    ->orderBy('o.created_at', 'DESC')
    ->limit(10)
    ->get();
/*
 * =======================FINISHED===================================================
 */
